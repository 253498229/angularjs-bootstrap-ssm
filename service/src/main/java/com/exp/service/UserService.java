package com.exp.service;

import com.exp.dao.more.MoreUserMapper;
import com.exp.model.more.MoreUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by P0015475 on 2015/9/11.
 */
@Service
@Transactional
public class UserService {
  @Autowired
  private MoreUserMapper moreUserMapper;

  public List<MoreUser> getList() {
    return moreUserMapper.getList();
  }

  public void deleteUserRole(Integer roleId) {
    moreUserMapper.deleteUserRole(roleId);
  }
}

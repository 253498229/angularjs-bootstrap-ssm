# 郑重声明！！！！！
# 这个版本已经过时了，不建议大家参考了！！！！！！

##angularjs-bootstrap-ssm

#简单CRUD模版。 前台用angularjs+bootstrap，后台用springmvc+mybatis。 供新手参考 ，随时更新。

#MySQL数据库，SQL文件路径:web/src/main/resources/ssm.sql


#以下是学习中遇到的坑:

1:angularjs的route（路由）需要web容器，可以用tomcat，或者ide(eclipse、idea)的static web project
    也可以用chrome浏览器快捷方式加参数的方法实现，参数为:--allow-file-access-from-files

2:angularjs的$http服务中的Post请求问题。angularjs默认post请求传递的参数是json格式，这样如果后台用springmvc直接接收对象就会取不到。
    所以要在后台接收的对象前面加上@RequestBody，或者就加上web/webapp/resources/js/defaultHeader.js，
    将json格式转换，后台就可以直接接受对象了。推荐最后一种方法，一劳永逸，而且用法接近jquery。

3:service(provider,factory)问题，如果有一段controller代码可以重复利用，可以将之提取成service，但请不要试图在service中
    操作$scope
package com.exp.dao.more;

import com.exp.model.more.MoreUser;

import java.util.List;

/**
 * Created by P0015475 on 2015/9/11.
 */
public interface MoreUserMapper {
  List<MoreUser> getList();

  void deleteUserRole(Integer roleId);
}

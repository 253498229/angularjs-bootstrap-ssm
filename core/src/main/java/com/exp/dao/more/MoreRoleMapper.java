package com.exp.dao.more;

import com.exp.model.Role;

import java.util.List;

/**
 * Created by P0015475 on 2015/9/11.
 */
public interface MoreRoleMapper {
    List<Role> getList();
}

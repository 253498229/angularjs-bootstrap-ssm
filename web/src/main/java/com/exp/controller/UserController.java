package com.exp.controller;

import com.exp.dao.UserMapper;
import com.exp.model.User;
import com.exp.model.more.MoreUser;
import com.exp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by P0015475 on 2015/9/11.
 */
@RestController
@RequestMapping("/user")
public class UserController {
  @Autowired
  private UserService userService;
  @Autowired
  private UserMapper  userMapper;


  @RequestMapping(method = RequestMethod.GET)
  public List<MoreUser> list() {
    List<MoreUser> users = userService.getList();
    return users;
  }

  @RequestMapping(value = "/{id}", method = RequestMethod.GET)
  public User getUser(@PathVariable Integer id) {
    User user = null;
    if (id != null) {
      user = userMapper.selectByPrimaryKey(id);
    }
    return user;
  }

  @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
  public boolean delete(@PathVariable Integer id) {
    userMapper.deleteByPrimaryKey(id);
    return true;
  }

  @RequestMapping(method = RequestMethod.POST)
  public boolean edit(@RequestBody User user) {
    if (user.getId() != null) {
      userMapper.updateByPrimaryKeySelective(user);
    } else {
      userMapper.insertSelective(user);
    }
    return true;
  }
}

package com.exp.controller;

import com.exp.dao.RoleMapper;
import com.exp.dao.more.MoreRoleMapper;
import com.exp.model.Role;
import com.exp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by WangBin on 2015/9/11.
 */
@RestController
@RequestMapping("/role")
public class RoleController {
  @Autowired
  private RoleMapper     roleMapper;
  @Autowired
  private MoreRoleMapper moreRoleMapper;
  @Autowired
  private UserService    userService;

  @RequestMapping(method = RequestMethod.GET)
  public List<Role> list() {
    List<Role> roles = moreRoleMapper.getList();
    return roles;
  }

  @RequestMapping(value = "/{id}", method = RequestMethod.GET)
  public Role getRole(@PathVariable Integer id) {
    Role role = null;
    if (id != null) {
      role = roleMapper.selectByPrimaryKey(id);
    }
    return role;
  }

  @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
  public boolean delete(@PathVariable Integer id) {
    roleMapper.deleteByPrimaryKey(id);
    userService.deleteUserRole(id);
    return true;
  }

  @RequestMapping(method = RequestMethod.POST)
  public boolean edit(@RequestBody Role role) {
    if (role.getId() != null) {
      roleMapper.updateByPrimaryKeySelective(role);
    } else {
      roleMapper.insertSelective(role);
    }
    return true;
  }
}

'use strict';

function getContextPath() {
  var pathName = document.location.pathname;
  var index = pathName.substr(1).indexOf("/");
  var result = pathName.substr(0, index + 1);
  return result;
}

var ctx = window.location.origin + getContextPath();
angular.module('app', [
  // 'ui.bootstrap',
  'ngSanitize',
  'ui.router',
  'oc.lazyLoad',
  'app.lazyload',
  'app.routes',
  'ngStorage',
  //业务模块
  'role',//角色
  'user',//用户
])
    .run(
        ['$rootScope', '$state', '$stateParams',
          function ($rootScope, $state, $stateParams) {
            $rootScope.$state = $state;
            $rootScope.$stateParams = $stateParams;
          }])
    .controller('appCtrl', function ($scope, $rootScope, $http) {
      $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
        $scope.url = toState.url.replaceAll('/', '').replaceAll('-', '');
      });
    });

angular.module('app.lazyload', []);
angular.module('app.routes', []);
//业务模块
angular.module('role', []);
angular.module('user', []);

/**
 * 替换URL方法
 */
String.prototype.replaceAll = function (s1, s2) {
  var temp = this;
  while (temp.indexOf(s1) != -1) {
    temp = temp.replace(s1, s2);
  }
  return temp;
}

angular.module('user').controller('userEditCtrl', function ($scope, $http, $stateParams, $state) {
  $http.get(ctx + '/role').then(function (resp) {
    $scope.roles = resp.data;
  });
  if ($stateParams.id) {
    $http.get(ctx + '/user/' + $stateParams.id).then(function (resp) {
      $scope.user = resp.data;
    });
  }
  $scope.saveUser = function () {
    $http.post(ctx + '/user', $scope.user).then(function (resp) {
      if (resp.data == true) {
        if ($stateParams.id)
          alert('修改成功');
        else
          alert('新建成功');
        $state.go('app.userList');
      }
    });
  }
});
angular.module('user').controller('userListCtrl', function ($scope, $http, $state) {
  $scope.users = [];
  $scope.onInit = function () {
    $http.get(ctx + '/user').then(function (resp) {
      $scope.users = resp.data;
    });
  }
  $scope.onInit();
  $scope.deleteUser = function (id, name) {
    if (confirm('确定要删除"' + name + '"吗?')) {
      $http.delete('/user/' + id).then(function (resp) {
        if (resp.data == true) {
          alert('删除成功');
          $scope.onInit();
        } else {
          alert('删除失败');

        }
      });
    }
  }
});
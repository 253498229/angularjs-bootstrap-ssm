angular.module('role').controller('roleListCtrl', function ($scope, $http) {
  $scope.roles = [];
  $scope.onInit = function () {
    $http.get(ctx + '/role').then(function (resp) {
      $scope.roles = resp.data;
    });
  }
  $scope.onInit();
  $scope.deleteRole = function (id, name) {
    if (confirm('确定要删除"' + name + '"吗?')) {
      $http.delete('/role/' + id).then(function (resp) {
        if (resp.data == true) {
          alert('删除成功');
          $scope.onInit();
        } else {
          alert('删除失败');

        }
      });
    }
  }
});
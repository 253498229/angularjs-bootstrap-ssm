angular.module('role').controller('roleEditCtrl', function ($scope, $http, $stateParams, $state) {
  if ($stateParams.id) {
    $http.get(ctx + '/role/' + $stateParams.id).then(function (resp) {
      $scope.role = resp.data;
    });
  }
  $scope.saveRole = function () {
    $http.post(ctx + '/role', $scope.role).then(function (resp) {
      if (resp.data == true) {
        if ($stateParams.id)
          alert('修改成功');
        else
          alert('新建成功');
        $state.go('app.roleList');
      }
    });
  }
});
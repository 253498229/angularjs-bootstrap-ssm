'use strict';
angular
    .module('app.lazyload')
    .config(['$compileProvider', function ($compileProvider) {
      $compileProvider.imgSrcSanitizationWhitelist(/^\s*(http|https|data|wxlocalresource|weixin):/);
    }])
    .constant('APP_REQUIRES', {
      scripts: {
        'sexFilter': ['app/common/filter/sex.filter.js']
      },
      modules: [
        {
          name: 'roleList',
          files: ['app/modules/role/role.list.js']
        },
        {
          name: 'roleEdit',
          files: ['app/modules/role/role.edit.js']
        },
        {
          name: 'userList',
          files: ['app/modules/user/user.list.js']
        },
        {
          name: 'userEdit',
          files: ['app/modules/user/user.edit.js']
        }
      ]
    });

'use strict';
angular.module('app').config([
  '$stateProvider', '$urlRouterProvider', '$locationProvider', 'RouteHelpersProvider',
  function ($stateProvider, $urlRouterProvider, $locationProvider, helper) {
    $urlRouterProvider.otherwise('/app/home');
    $stateProvider
        .state('app', {
          abstract: true,
          url: '/app',
          templateUrl: helper.basepath('common/layout.html')
        })
        .state('app.home', {
          url: '/home',
          templateUrl: helper.basepath('common/home.html')
        })
        .state('app.roleList', {
          url: '/roleList',
          templateUrl: helper.basepath('modules/role/role.list.html'),
          resolve: helper.resolveFor('roleList'),
          controller: 'roleListCtrl'
        })
        .state('app.roleEdit', {
          url: '/roleEdit/:id',
          templateUrl: helper.basepath('modules/role/role.edit.html'),
          resolve: helper.resolveFor('roleEdit'),
          controller: 'roleEditCtrl'
        })
        .state('app.userList', {
          url: '/userList',
          templateUrl: helper.basepath('modules/user/user.list.html'),
          resolve: helper.resolveFor('userList','sexFilter'),
          controller: 'userListCtrl'
        })
        .state('app.userEdit', {
          url: '/userEdit/:id',
          templateUrl: helper.basepath('modules/user/user.edit.html'),
          resolve: helper.resolveFor('userEdit'),
          controller: 'userEditCtrl'
        })
  }]);